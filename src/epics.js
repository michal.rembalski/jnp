import { combineEpics } from 'redux-observable';
import { getLocationEpic } from './containers/location/epics';
import { fetchCityEpic } from './containers/search-bar/epics'
import { getWeatherEpic } from './containers/weather/epics';
import { fetchWeatherEpic } from './containers/weather-view/epics';
export const rootEpic = combineEpics(fetchCityEpic, getLocationEpic, getWeatherEpic, fetchWeatherEpic);
