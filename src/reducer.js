import { combineReducers } from 'redux';
import { SEARCH_REDUCER_NAME, searchReducer } from './containers/search-bar/reducer';
import { TIMESPAN_REDUCER_NAME, timespanReducer } from './containers/timespan/reducer';
import { THEME_REDUCER_NAME, themeReducer } from './containers/theme/reducer';
import { WEATHER_REDUCER_NAME, weatherReducer } from './containers/weather/reducer';
import { GIF_REDUCER_NAME, gifReducer } from './containers/weather-view/reducer';

export default function createReducer() {
    return combineReducers({
        [THEME_REDUCER_NAME]: themeReducer,
        [SEARCH_REDUCER_NAME]: searchReducer,
        [TIMESPAN_REDUCER_NAME]: timespanReducer,
        [WEATHER_REDUCER_NAME]: weatherReducer,
        [GIF_REDUCER_NAME]: gifReducer,
    });
}
