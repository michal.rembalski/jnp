import { createSelector } from 'reselect';
import * as R from 'ramda';
import { WEATHER_REDUCER_NAME } from './reducer';

const getWeatherReducerState = R.prop(WEATHER_REDUCER_NAME);

export const weatherSelector = createSelector(
    getWeatherReducerState,
    (weather) => weather.get("weather")
);

export const loadingSelector = createSelector(
    getWeatherReducerState,
    (weather) => weather.get("isLoading")
);
