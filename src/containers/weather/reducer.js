import { fromJS } from 'immutable';
import { CONSTANTS } from './const';

export const WEATHER_REDUCER_NAME = 'weather';

const initialState = fromJS({
    weather: null,
    isLoading: false
});

export const weatherReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANTS.GET_WEATHER:
            return state
                .set('isLoading', true);
        case CONSTANTS.FETCH_WEATHER:
            return state
                .set('weather', action.weather)
                .set('isLoading', false);
        default:
            return state;
    }
};