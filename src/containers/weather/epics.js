
import { CONSTANTS } from './const';
import { ofType } from 'redux-observable';
import { catchError, map, mergeMap, takeUntil } from 'rxjs/operators';
import { fetchWeather } from './actions';
import { ajax } from 'rxjs/ajax';
import { EMPTY } from 'rxjs';
import { CONSTANTS as SEARCH_BAR_CONSTANTS } from '../search-bar/const'

const getUrl = (timespan, place) => {
    if (timespan === "ONE_DAY") {
        return `http://api.weatherapi.com/v1/forecast.json?key=${CONSTANTS.API_KEY}&q=${place}&days=1&aqi=no&alerts=no`
    }
    if (timespan === "THREE_DAYS") {
        return `http://api.weatherapi.com/v1/forecast.json?key=${CONSTANTS.API_KEY}&q=${place}&days=3&aqi=no&alerts=no`
    }
    if (timespan === "REALTIME") {
        return `http://api.weatherapi.com/v1/current.json?key=${CONSTANTS.API_KEY}&q=${place}`
    }
}

export const getWeatherEpic = (action$) =>
    action$.pipe(
        ofType(CONSTANTS.GET_WEATHER),
        mergeMap(action =>
            ajax.getJSON(getUrl(action.timespan, action.place))
                .pipe(
                    map(fetchWeather),
                    catchError(() => EMPTY),
                    takeUntil(
                        action$.pipe(
                            ofType(SEARCH_BAR_CONSTANTS.CITY_WRITE),
                        )
                    )
                )
        )
    );
