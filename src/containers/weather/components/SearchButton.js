import React from 'react';
import { getWeather } from '../actions';
import { Button } from '../../../components/Button'
import { searchSelector } from '../../search-bar/selectors';
import { timespanSelector } from '../../timespan/selectors';
import { useDispatch, useSelector } from 'react-redux';

export const SearchButton = () => {
    const dispatch = useDispatch();
    const searchValue = useSelector(searchSelector);
    const timespanValue = useSelector(timespanSelector)

    return (
        <Button onClick={() => dispatch(getWeather(searchValue, timespanValue))}>
            Search
        </Button>
    );
}