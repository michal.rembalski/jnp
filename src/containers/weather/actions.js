import { CONSTANTS } from './const.js';

export const getWeather = (place, timespan) => ({
    type: CONSTANTS.GET_WEATHER,
    place: place,
    timespan: timespan
});

export const fetchWeather = (weather) => ({
    type: CONSTANTS.FETCH_WEATHER,
    weather: weather,
    text: weather.current.condition.text
});
