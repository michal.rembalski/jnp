import React from 'react';
import { useDispatch } from 'react-redux';
import { getLocation } from '../actions';
import { Button } from '../../../components/Button'
export const LocationButton = () => {
    const dispatch = useDispatch();

    return (
        <Button onClick={() => dispatch(getLocation())}>
            Location
        </Button>
    );
}