import { CONSTANTS } from './const';

export const getLocation = () => ({
    type: CONSTANTS.GET_LOCATION,
});