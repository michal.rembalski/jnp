
import { Observable } from 'rxjs'
import { CONSTANTS } from '../location/const';
import { CONSTANTS as SEARCH_BAR_CONSTANTS } from '../search-bar/const'
import { ofType } from 'redux-observable';
import { map, mergeMap, takeUntil } from 'rxjs/operators';
import { setCity } from '../search-bar/actions';

const currentPosition$ = new Observable(obs => {
    navigator.geolocation.getCurrentPosition(
        success => {
            obs.next(success);
            obs.complete();
        },
        error => {
            obs.error(error);
        }
    );
});

export const getLocationEpic = (action$) =>
    action$.pipe(
        ofType(CONSTANTS.GET_LOCATION),
        mergeMap(() =>
            currentPosition$
                .pipe(
                    map(
                        (position) => setCity(
                            `${position.coords.latitude}, ${position.coords.longitude}`
                        )
                    ),
                    takeUntil(
                        action$.pipe(
                            ofType(SEARCH_BAR_CONSTANTS.CITY_SET, SEARCH_BAR_CONSTANTS.CITY_WRITE)
                        )
                    )
                )
        )
    );
