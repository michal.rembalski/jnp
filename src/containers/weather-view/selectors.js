import { createSelector } from 'reselect';
import * as R from 'ramda';
import { GIF_REDUCER_NAME } from './reducer';

const getGifReducerState = R.prop(GIF_REDUCER_NAME);

export const gifSelector = createSelector(
    getGifReducerState,
    (st) => st.get("gif")
);
