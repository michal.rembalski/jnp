import React from 'react';
import { useSelector } from 'react-redux';
import { weatherSelector } from '../../weather/selectors';
import { gifSelector } from '../selectors';

let getAverageTemp = arr => {
    let temps = arr.forecastday.map(day => day.hour.map(hour => hour.temp_c));
    temps = [].concat.apply([], temps);
    let reducer = (total, currentValue) => total + currentValue;
    let sum = temps.reduce(reducer)
    console.log("averageTemp", sum / temps.length)

    return sum / temps.length;
}

let getMinTemp = arr => {
    let temps = arr.forecastday.map(day => day.hour.map(hour => hour.temp_c));
    temps = [].concat.apply([], temps);
    console.log("minTemp", Math.min(...temps))

    return Math.min(...temps);
}

let getMaxTemp = arr => {
    let temps = arr.forecastday.map(day => day.hour.map(hour => hour.temp_c));
    temps = [].concat.apply([], temps);
    console.log("maxTemp", Math.max(...temps))

    return Math.max(...temps);
}

let anyRainyDay = arr => {
    let rains = arr.forecastday.map(day => day.hour.map(hour => hour.will_it_rain));
    rains = [].concat.apply([], rains);
    console.log("maxRain", Math.max(...rains))

    return Math.max(...rains) > 0;
}

let allThree = (a, b, c) => {
    return a && b && c
}

let onlyTwo = (a, b, c) => {
    return (a && b && !c) || (a && c && !b) || (b && c && !a);
}

let onlyOneOrZero = (a, b, c) => {
    return (a && !b && !c) || (!a && b && !c) || (!a && !b && c) || (!a && !b && !c)
}


export const WeatherView = () => {
    const weatherValue = useSelector(weatherSelector);
    const gifLink = useSelector(gifSelector);

    return (
        <table>
            <tr>
                <td>
                    {
                        weatherValue && (
                            <div>
                                <h5>
                                    Current temperature
                                </h5>
                                <a>
                                    {weatherValue.current.temp_c}
                                </a>
                            </div>
                        )
                    }
                </td>
                <td>
                    {
                        weatherValue && weatherValue.forecast && (
                            <div>
                                <h5>
                                    Hourly temperatures
                                </h5>
                                {weatherValue.forecast.forecastday.map((day) => {
                                    return (
                                        day.hour.map((hour) => {
                                            return (
                                                <div>
                                                    <a>{hour.temp_c}<br /></a>
                                                </div>
                                            )
                                        })
                                    )
                                })}
                            </div>

                        )
                    }
                </td>
                <td>
                    {
                        weatherValue && weatherValue.forecast &&
                        allThree(
                            !anyRainyDay(weatherValue.forecast),
                            getAverageTemp(weatherValue.forecast) >= 18 && getAverageTemp(weatherValue.forecast) <= 25,
                            getMinTemp(weatherValue.forecast) >= 15 && getMaxTemp(weatherValue.forecast) <= 30
                        ) && (
                            <div>
                                <h5>
                                    Niceness
                                </h5>
                                <a>NICE<br /></a>
                            </div>
                        )
                    }
                </td>
                <td>
                    {
                        weatherValue && weatherValue.forecast &&
                        onlyTwo(
                            !anyRainyDay(weatherValue.forecast),
                            getAverageTemp(weatherValue.forecast) >= 18 && getAverageTemp(weatherValue.forecast) <= 25,
                            getMinTemp(weatherValue.forecast) >= 15 && getMaxTemp(weatherValue.forecast) <= 30
                        ) && (
                            <div>
                                <h5>
                                    Niceness
                                </h5>
                                <a>PASSABLE<br /></a>
                            </div>
                        )
                    }
                </td>
                <td>
                    {
                        weatherValue && weatherValue.forecast &&
                        onlyOneOrZero(
                            !anyRainyDay(weatherValue.forecast),
                            getAverageTemp(weatherValue.forecast) >= 18 && getAverageTemp(weatherValue.forecast) <= 25,
                            getMinTemp(weatherValue.forecast) >= 15 && getMaxTemp(weatherValue.forecast) <= 30
                        ) && (
                            <div>
                                <h5>
                                    Niceness
                                </h5>
                                <a>NOT NICE<br /></a>
                            </div>
                        )
                    }
                </td>
                <td>
                    {
                        gifLink && (
                            <div>
                                <h5>
                                    Gif for fun
                                </h5>
                                <img src={gifLink} />
                            </div>
                        )
                    }
                </td>
            </tr>
        </table>
    );
}