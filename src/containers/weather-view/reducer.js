import { fromJS } from 'immutable';
import { CONSTANTS } from './const';

export const GIF_REDUCER_NAME = 'gif';

const initialState = fromJS({
    gif: null
});

export const gifReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANTS.FETCH_GIF:
            console.log(action.gifs)
            return state
                .set('gif', action.gifs.results[0].media[0].gif.url);
        default:
            return state;
    }
};