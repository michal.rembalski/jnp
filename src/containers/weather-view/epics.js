import { CONSTANTS as WEATHER_CONSTANTS } from '../weather/const';
import { CONSTANTS } from './const';
import { ofType } from 'redux-observable';
import { catchError, map, mergeMap, takeUntil } from 'rxjs/operators';
import { fetchGif } from './actions';
import { ajax } from 'rxjs/ajax';
import { EMPTY } from 'rxjs';
import { CONSTANTS as SEARCH_BAR_CONSTANTS } from '../search-bar/const'

const getUrl = (text) => {
    console.log(`https://g.tenor.com/v1/search?q=${text.replaceAll(' ', '-')}-weather&key=${CONSTANTS.API_KEY}`)
    return `https://g.tenor.com/v1/search?q=${text.replaceAll(' ', '-')}-weather&key=${CONSTANTS.API_KEY}`
}

export const fetchWeatherEpic = (action$) =>
    action$.pipe(
        ofType(WEATHER_CONSTANTS.FETCH_WEATHER),
        mergeMap(action =>
            ajax.getJSON(getUrl(action.text))
                .pipe(
                    map(fetchGif),
                    catchError(() => EMPTY),
                    takeUntil(
                        action$.pipe(
                            ofType(SEARCH_BAR_CONSTANTS.CITY_WRITE),
                        )
                    )
                )
        )
    );