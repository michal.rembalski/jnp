import { CONSTANTS } from './const.js';

export const fetchGif = (gifs) => ({
    type: CONSTANTS.FETCH_GIF,
    gifs: gifs
});
