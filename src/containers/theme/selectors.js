import { createSelector } from 'reselect';
import * as R from 'ramda';
import { THEME_REDUCER_NAME } from './reducer';

const getThemeReducerState = R.prop(THEME_REDUCER_NAME);

export const themeSelector = createSelector(
    getThemeReducerState,
    (state) => state.get("theme")
);