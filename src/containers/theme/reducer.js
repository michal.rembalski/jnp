import { fromJS } from 'immutable';
import { THEMES } from './index';
import { CONSTANTS } from './const';

export const THEME_REDUCER_NAME = 'theme';

const initialState = fromJS({
    theme: THEMES.LIGHT,
});

const getOppositeTheme = (theme) => {
    if (theme === THEMES.LIGHT)
        return THEMES.DARK
    return THEMES.LIGHT
}

export const themeReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANTS.CHANGE_THEME:
            return state
                .update('theme', getOppositeTheme)
        default:
            return state;
    }
};