import React from 'react';
import { useDispatch } from 'react-redux';
import { changeTheme } from '../actions';
import { Button } from '../../../components/Button'

export const ThemeButton = () => {
    const dispatch = useDispatch();

    return (
        <Button onClick={() => dispatch(changeTheme())}>
            CHANGE THEME
        </Button>
    );
}