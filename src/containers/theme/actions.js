import { CONSTANTS } from './const.js';

export const changeTheme = () => ({
    type: CONSTANTS.CHANGE_THEME
});
