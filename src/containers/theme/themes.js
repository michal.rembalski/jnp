const light_blue = "#89D2DC"
const dark_blue = "#232ED1"

export const THEMES = {
    DARK: "DARK",
    LIGHT: "LIGHT"
}

export const THEMES_HTML = {
    DARK: {
        background: dark_blue,
        button: {
            color: light_blue
        }
    },
    LIGHT: {
        background: light_blue,
        button: {
            color: dark_blue
        }
    },
}
