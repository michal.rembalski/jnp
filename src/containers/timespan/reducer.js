import { fromJS } from 'immutable';
import { CONSTANTS } from './const';

export const TIMESPAN_REDUCER_NAME = 'timespan';

const initialState = fromJS({
    timespan: 'REALTIME',
});

export const timespanReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANTS.REALTIME:
            return state
                .set('timespan', "REALTIME")
        case CONSTANTS.ONE_DAY:
            return state
                .set('timespan', "ONE_DAY");
        case CONSTANTS.THREE_DAYS:
            return state
                .set('timespan', "THREE_DAYS");
        default:
            return state;
    }
};