import React from 'react';
import { useDispatch } from 'react-redux';
import { setRealtime, setThreeDays, setOneDay } from '../actions';
import { HoldButton } from '../../../components'

export const RealtimeButton = () => {
    const dispatch = useDispatch();

    return (
        <HoldButton onClick={() => dispatch(setRealtime())}>
            REALTIME
        </HoldButton>
    );
}

export const OneDayButton = () => {
    const dispatch = useDispatch();

    return (
        <HoldButton onClick={() => dispatch(setOneDay())}>
            ONE DAY
        </HoldButton>
    );
}

export const ThreeDaysButton = () => {
    const dispatch = useDispatch();

    return (
        <HoldButton onClick={() => dispatch(setThreeDays())}>
            THREE DAYS
        </HoldButton>
    );
}
