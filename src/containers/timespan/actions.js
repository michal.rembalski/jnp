import { CONSTANTS } from './const.js';

export const setRealtime = () => ({
    type: CONSTANTS.REALTIME
});

export const setThreeDays = () => ({
    type: CONSTANTS.THREE_DAYS
});

export const setOneDay = () => ({
    type: CONSTANTS.ONE_DAY
});
