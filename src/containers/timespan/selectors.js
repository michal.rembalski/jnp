import { createSelector } from 'reselect';
import * as R from 'ramda';
import { TIMESPAN_REDUCER_NAME } from './reducer';

const getTimespanReducerState = R.prop(TIMESPAN_REDUCER_NAME);

export const timespanSelector = createSelector(
    getTimespanReducerState,
    (state) => state.get("timespan")
);