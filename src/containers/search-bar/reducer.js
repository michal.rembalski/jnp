import { fromJS } from 'immutable';
import { CONSTANTS } from './const';

export const SEARCH_REDUCER_NAME = 'search';

const initialState = fromJS({
    search: '',
    suggestions: []
});

export const searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANTS.CITY_SET:
            return state
                .set('search', action.city)
                .set('suggestions', fromJS([]));
        case CONSTANTS.CITY_WRITE:
            return state
                .set('search', action.city);
        case CONSTANTS.CITY_FETCH:
            return state
                .set('suggestions', fromJS(action.suggestions));
        default:
            return state;
    }
};