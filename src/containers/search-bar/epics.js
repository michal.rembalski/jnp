import { ofType } from 'redux-observable';
import { ajax } from 'rxjs/ajax';
import { catchError, map, mergeMap, takeUntil } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { CONSTANTS } from './const';
import { fetchCity } from './actions';

export const fetchCityEpic = (action$) =>
    action$.pipe(
        ofType(CONSTANTS.CITY_WRITE),
        mergeMap(action =>
            ajax.getJSON(`${CONSTANTS.API_URL}/search.json?key=${CONSTANTS.API_KEY}&q=${action.city}`)
                .pipe(
                    map(fetchCity),
                    catchError(() => EMPTY),
                    takeUntil(
                        action$.pipe(
                            ofType(CONSTANTS.CITY_WRITE),
                        )
                    )
                )
        )
    );
