import React from 'react';
import Autocomplete from 'react-autocomplete';
import { useDispatch, useSelector } from 'react-redux';
import { setCity, writeCity } from '../actions';
import { searchSelector, suggestionsSelector } from '../selectors';

export const SearchBar = () => {
    const dispatch = useDispatch();
    const searchValue = useSelector(searchSelector);
    const suggestionsValue = useSelector(suggestionsSelector);

    return (
        <Autocomplete
            value={searchValue}
            getItemValue={(suggestion) => suggestion.name}
            items={suggestionsValue}
            onChange={(newValue) => {
                const value = newValue.target.value;
                dispatch(writeCity(value));
            }}
            onSelect={(newValue) => {
                const value = newValue;
                dispatch(setCity(value));
            }}
            renderItem={(item, isHighlighted) => (
                <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                    {item.name}
                </div>
            )}
        />
    );
}