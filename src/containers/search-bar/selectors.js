import { createSelector } from 'reselect';
import * as R from 'ramda';
import { SEARCH_REDUCER_NAME } from './reducer';

const getSearchReducerState = R.prop(SEARCH_REDUCER_NAME);

export const searchSelector = createSelector(
    getSearchReducerState,
    (search) => search.get("search")
);

export const suggestionsSelector = createSelector(
    getSearchReducerState,
    (search) => search.get("suggestions").toJS()
);