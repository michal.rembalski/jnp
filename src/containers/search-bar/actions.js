import { CONSTANTS } from './const';

export const setCity = (city) => ({
    type: CONSTANTS.CITY_SET,
    city: city
});

export const writeCity = (city) => ({
    type: CONSTANTS.CITY_WRITE,
    city: city
});

export const fetchCity = (suggestions) => ({
    type: CONSTANTS.CITY_FETCH,
    suggestions: suggestions
});
