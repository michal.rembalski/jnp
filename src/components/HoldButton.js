import styled from 'styled-components';
import { theme } from 'styled-tools';

export const HoldButton = styled.button`
  font-size: 15px;
  margin: 2px;
  padding: 2px 4px;
  border-radius: 3px;
  width: 200px;
  align:center;
  :active {
    color: grey;
  }
  :focus{background-color:red;}

  color:${theme('button.color')};
  border: 2px solid ${theme('button.color')};
`;
