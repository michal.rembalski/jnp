import styled from 'styled-components';
import { theme } from 'styled-tools';

export const Button = styled.button`
  font-size: 15px;
  margin: 2px;
  padding: 2px 4px;
  border-radius: 3px;
  width: 200px;
  align:center;
  :active {
    color: grey;
  }

  color:${theme('button.color')};
  border: 2px solid ${theme('button.color')};
`;
