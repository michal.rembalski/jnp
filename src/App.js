import { React } from 'react';
import { SearchBar } from "./containers/search-bar"
import { OneDayButton, RealtimeButton, ThreeDaysButton } from "./containers/timespan"
import { LocationButton } from "./containers/location"
import { ThemeButton } from './containers/theme';
import { themeSelector } from './containers/theme/selectors';
import { useSelector } from 'react-redux';
import { ThemeProvider } from "styled-components";
import { THEMES_HTML } from "./containers/theme/index"
import { GlobalStyle } from './containers/theme/global-styles';
import { SearchButton } from './containers/weather';
import { WeatherView } from './containers/weather-view';
import { CustomLoader } from './components/CustomLoader';
import { loadingSelector } from './containers/weather/selectors';

export function App() {
  const theme = useSelector(themeSelector);
  const isLoading = useSelector(loadingSelector);

  if (isLoading)
    return (
      <CustomLoader />
    )
  else
    return (
      <ThemeProvider theme={THEMES_HTML[theme]}>
        <GlobalStyle />
        <SearchBar />
        <ThemeButton />
        <OneDayButton />
        <RealtimeButton />
        <ThreeDaysButton />
        <LocationButton />
        <SearchButton />
        <br />
        <WeatherView />
      </ThemeProvider>
    )
}